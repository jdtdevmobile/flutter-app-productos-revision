import 'package:flutter/cupertino.dart';

class LoginFormProvider extends ChangeNotifier {

  GlobalKey <FormState> formKey = new GlobalKey<FormState>();

  String email      = '';
  String password   = ''; 

  //La propieda _isLoading con su get y set permitira llamar a notifyListener()  y al llamalo va a redibujar el arbol de widgets basado en lo que cambio.
  bool _isLoading = false;
  bool get isLoading => _isLoading;

  set isLoading( bool value ) {
    _isLoading = value;
    notifyListeners(); //Hara notificacion de los cambios de los widgets de loginFormProvider
  } 


  bool isValidForm(){

    print(formKey.currentState?.validate());
    
    print('$email - $password');

    
    return formKey.currentState?.validate() ?? false;
  }
}