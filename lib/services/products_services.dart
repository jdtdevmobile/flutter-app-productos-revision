import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:login_app/models/models.dart';
import 'package:http/http.dart' as http;




class ProductsService extends ChangeNotifier {

  final String _baseUrl = 'flutter-varios-65b5f-default-rtdb.firebaseio.com';
  final List<Product> products =  []; //Es final porque no se quiere destruir el objeto, Solo reasignar sus valores internos
  late Product selectedProduct ;
  bool isLoading = true; //Propiedad que me permite  saber cuando estoy cargando algo
  bool isSaving = false;

  ProductsService() {
  loadProducts();
  }


  Future <List<Product>> loadProducts() async {

    isLoading = true; //el this lo quito Arles porque no se requiere 
    notifyListeners();

    final url = Uri.https( _baseUrl, 'products.json'); //Esta line hace l peticion al endpoint
    final resp = await http.get(url); //Aca se obtiene la respuesta.

    final Map<String, dynamic> productsMap = json.decode( resp.body ); //se decodifica la respuesta esta respuesta es un json y al decodificarse de pasa a un string y se deja lista para ser mostrada 
    
    print(productsMap);

    productsMap.forEach((key, value)async {
      final tempProduct = Product.fromMap(value);
      tempProduct.id = key;
      products.add( tempProduct );
    }); 

     isLoading = false;
    notifyListeners();

      return products;
    //print ( this.products[0].name ); 



  }

  Future saveOrCreateProduct ( Product product ) async {

    isSaving = true;
    notifyListeners();

    if(product.id == null ) { //que el id sea null es lo que me permite decidir si el producto existe o no 
      // es necesario crear, se deja para la siguiente clase 
    }else{
      //para actualizar se llama a un nuevo metodo que se encuentra lineas mas abajo.
      await updateProduct(product);
    }

    isSaving = false;
    notifyListeners();

  }
  //Nos creamos otro metodo future que se encargara de hacer la peticion a firebase para actualizar el id del producto
  Future<String> updateProduct ( Product product) async {

    final url = Uri.https(_baseUrl, 'products/${ product.id }.json');
    final resp = await http.put ( url, body: product.toJson()  );
    final decodeData = resp.body;

    print(decodeData);

    //TODO: Actualizar listado de productos.

    final index = products.indexWhere((element) => element.id == product.id);// Regresa el indice del producto cuyo id es = al indice del producto que esta recibiendo en el parametro de este metodo updateProduct
    products[index] = product; //Luego voy al indice que acabo de extraer y lo asigno al product, con esto ya se ve reflejado en la pnatalla principal los cambios realizados.

    return product.id!;

  }



}