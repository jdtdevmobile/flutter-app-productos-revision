import 'package:flutter/material.dart';
import 'package:login_app/screens/screens.dart';
import 'package:login_app/services/products_services.dart';
import 'package:provider/provider.dart';

void main() =>  runApp( const AppState());


class AppState extends StatelessWidget {
   const AppState({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [

        ChangeNotifierProvider(create: ( _ ) =>  ProductsService())

      ],
      child: MyApp(),
      
    );
  }
}

class   MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Productos App',
      initialRoute: 'home',
      routes: {
        'login'   : ( _ ) => const LoginScreen(),
        'home'    : ( _ ) => const HomeScreen(),
        'product' : ( _ ) => const ProductScreen()
      },
      theme: ThemeData.light().copyWith(
        scaffoldBackgroundColor: Colors.grey [300],
        appBarTheme: const AppBarTheme(
          elevation: 0,
          color: Colors.indigo
        ),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: Colors.indigoAccent,
          elevation: 0
        )

      )
      
    );
  }
}