// To parse this JSON data, do
//
//     final product = productFromMap(jsonString);

import 'dart:convert';

class Product {
    Product({
      required  this.available,
      required  this.name,
      this.picture,
      required this.price,
      this.id
      
         //this.productName,
    });

    bool available;
    String  name;
    String? picture;
    double price;
    String? id;
    //String productName;

    factory Product.fromJson(String str) => Product.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Product.fromMap(Map<String, dynamic> json) => Product(
        available: json["available"],
        name: json['name'],
        picture: json["picture"],
        price: json["price"] 
       // productName: json["name "] == null ? null : json["name "],
    );

    Map<String, dynamic> toMap() => {
        "available": available,
        'name': name,
        "picture": picture,
        "price": price,
      //  "name ": productName == null ? null : productName,
    };

    Product copy()  =>  Product(
      available:  available,
      name: name,
      picture: picture,
      price: price,
      id: id,

    );
}