

import 'package:flutter/material.dart';
import 'package:login_app/providers/login_form_provider.dart';
import 'package:login_app/ui/input_decorations.dart';
import 'package:login_app/widgets/widgets.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AuthBackground(
        child: SingleChildScrollView(
            child: Column(children: [
            
              const SizedBox(height: 200),

              CardContainer(
                child: Column(
                  children: [
                    
                    const SizedBox(height: 10),
                    Text('Login',style: Theme.of(context).textTheme.headline4),
                    const SizedBox(height: 10),

                      //Para manejar el estado del formulario del login.
                      ChangeNotifierProvider(
                        create: ( _ ) => LoginFormProvider(), //Crea una instancia del LoginFormProvider y puede dibujar o redibujar los widgets cuando sea necesario
                        child: _LoginForm(),
                        )
                     
              
            ],
          )
          ),

          const SizedBox( height: 50,),
          const Text('Crear una nueva cuenta', 
          style:  TextStyle(fontSize: 18, fontWeight: FontWeight.bold ),),
           const SizedBox( height: 50,)
        ])
            // Text('Hola Fernando'),
            ),
      ),
    );
  }
}
 class _LoginForm extends StatelessWidget {
   //const _LoginForm({Key? key}) : super(key: key);
 
   @override
   Widget build(BuildContext context) {

     //Con el siguiente loginForm se puede acceder a todo lo que ofreece la clase LoginFormProvider
     final loginForm = Provider.of<LoginFormProvider>(context);

     return   Container(
       child: Form(
         // TODO: mantener la referencia al KEY
         key: loginForm.formKey ,
         autovalidateMode: AutovalidateMode.onUserInteraction,
         child: Column(
           children: [

             TextFormField(
               autocorrect: false,
               keyboardType: TextInputType.emailAddress,
               decoration: InputDecorations.authInputDecoration(
                 hintText: 'ejemplo.ejemplo@gmail.com',
                 labelText: 'Correo electronico',
                 prefixIcon: Icons.alternate_email_rounded
               ),
               onChanged: (value) => loginForm.email = value,
               validator: (value) {
                
                String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                RegExp regExp  = new RegExp(pattern);

                return regExp.hasMatch(value ?? '')
                ? null
                :'El valor ingresado no tiene un formato de correo';
               },
             ),

            const SizedBox(height: 30,),

             TextFormField(
               autocorrect: false,
               obscureText: true,
               keyboardType: TextInputType.emailAddress,
               decoration: InputDecorations.authInputDecoration(
                 hintText: '******',
                 labelText: 'Contraseña',
                 prefixIcon: Icons.lock_outline
               ),
               onChanged: (value) => loginForm.password = value,
               validator: (value) {
                
              return ( value != null && value.length >= 6)
                ? null
                : 'La contraseña debe contener minimo 6 caracteres';
                
               },
             ),

            const  SizedBox(height: 30,),

            MaterialButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              disabledColor: Colors.grey,
              elevation: 0,
              color: Colors.deepPurple,
              child:  Container(
                padding: const EdgeInsets.symmetric( horizontal: 80, vertical: 15),
               child:  Text(
                 //Esto es para cambiar el texto en el boton dependiendo del estado de la variable isLoading
                 loginForm.isLoading
                 ? 'Espere'
                 : 'Ingresar',
                style: TextStyle(color: Colors.white ),
               )
              ),
              onPressed: loginForm.isLoading ? null : () async {

                FocusScope.of(context).unfocus();
                
                //Si no es true al presionar el boton entonces no hace ninguna accion, si es true navega a la siguiente pantalla 
                if(!loginForm.isValidForm()) return;

                loginForm.isLoading = true;

                  //Este Future es para simular una peticion http 
                  await Future.delayed(Duration(seconds: 2));

                  loginForm.isLoading = false;
                
                //pushReplacementNamed Destruye el stack de las pantallas y deja la nueva pantalla solamente.
                Navigator.pushReplacementNamed(context, 'home');
              })

           ],)
         ),
     );
   }
 }