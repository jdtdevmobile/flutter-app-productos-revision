import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:login_app/providers/product_form_provider.dart';
import 'package:login_app/ui/input_decorations.dart';
import 'package:login_app/widgets/widgets.dart';
import 'package:provider/provider.dart';

import '../services/services.dart';


class ProductScreen extends StatelessWidget {
   
  const ProductScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {

    final productsService = Provider.of<ProductsService>(context);

    return  ChangeNotifierProvider(//Fue necesario refactorizar con este codigo debido a que el icono de la camara esta por fuera del widget TextFormFiel
              create: ( _ ) => ProductFormProvider( productsService.selectedProduct),
              child: _ProductScreenBody(productsService: productsService),
      );
    
  }
}

class _ProductScreenBody extends StatelessWidget {
  const _ProductScreenBody({
    Key? key,
    required this.productsService,
  }) : super(key: key);

  final ProductsService productsService;

  @override
  Widget build(BuildContext context) {

    final productForm = Provider.of<ProductFormProvider>(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                ProductImage(url:productsService.selectedProduct.picture),

                Positioned(
                  top: 60,
                  left: 20,
                  child: IconButton(
                    onPressed: () => Navigator.of(context).pop(),
                    icon: const  Icon(Icons.arrow_back_ios_new, size: 40, color: Colors.white,)
                    ) 
                  ),

                  Positioned(
                    top:60,
                    right: 35,
                    child: IconButton(
                      onPressed: () {
                       // TODO: Camara o galeria 
                      }, 
                      icon: const Icon(Icons.camera_alt_outlined, size: 40, color: Colors.white ),
                      )
                    ),
                  ],
                ),
                const _ProductForm(),
                const SizedBox(height: 10),   
              ],
        ),
      ),
     
      floatingActionButton: FloatingActionButton(
        child: const Icon( Icons.save_outlined),
        onPressed: () async {
          
        //TODO: Guardar producto
         // Un return no deja entrar a las instrucciones posteriores
         // revisar la negacion de este if no deberia estar esta validacion.
        if( !productForm.isValidForm() ) return;



        await productsService.saveOrCreateProduct(productForm.product);

      },
      ),
    );
  }
}

class _ProductForm extends StatelessWidget {
  const _ProductForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {


    final productForm = Provider.of<ProductFormProvider>(context); //para tener acceso al provider y tiene la informacion del producto que se acabo de seleccionar
    final product = productForm.product;

    return Padding(
      padding: const EdgeInsets.symmetric( horizontal:10),
      child: Container(
        //height: 220,
        height: 240,
        padding: EdgeInsets.symmetric(horizontal: 20),
        width: double.infinity,
        child: Form( 
         key: productForm.formkey, //referencia al formulario en el provider
         autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            children: [
              TextFormField(
                initialValue: product.name,
                onChanged: ( value ) => product.name = value, //se tiene el producto actualizado
                validator: ( value ) {
                  if(value == null || value.length < 1)
                  return 'El nombre es obligatorio';
                },
                decoration: InputDecorations.authInputDecoration(
                  hintText: 'Nombre del producto', 
                  labelText: 'Nombre'),
              ),

              const SizedBox(height: 0.5),

              TextFormField(
                initialValue: '${product.price}',
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'^(\d+)?\.?\d{0,2}'))
                ], //Se hace necesario interpolar porque pide un String el initialValue y el price es un double   
                onChanged: ( value ) {
                  if(double.tryParse(value)== null){
                    product.price = 0;
                  } else {
                    product.price = double.parse(value);
                  }
                },
                keyboardType: TextInputType.number,
                decoration: InputDecorations.authInputDecoration(
                  hintText: '\$150', 
                  labelText: 'Precio:'),
              ),

              const SizedBox(height: 30),

              SwitchListTile.adaptive(
                value: product.available,
                title: const  Text('Disponible'),
                activeColor: Colors.indigo,
               
              //onChanged: (value) => productForm.updateAvalability(value)
              
                onChanged: productForm.updateAvalability,
           
              
              ),


            const  SizedBox(height: 10),
            ],)
          ),
        decoration: _buildBoxDecoration(),
      ),
    );
  }

  BoxDecoration _buildBoxDecoration() =>  BoxDecoration(
    color: Colors.white,
    borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(25), bottomRight: Radius.circular(25)),
    boxShadow: [
      BoxShadow(
        color: Colors.black.withOpacity(0.05),
        offset: const Offset(0,5),
        blurRadius: 5
      )
    ]
  );
}