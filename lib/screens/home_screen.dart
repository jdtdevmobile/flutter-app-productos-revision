import 'package:flutter/material.dart';
import 'package:login_app/screens/screens.dart';
import 'package:login_app/services/services.dart';
import 'package:provider/provider.dart';
import 'package:login_app/widgets/product_card.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final productsService = Provider.of<ProductsService>(context);

    if (productsService.isLoading) return const LoadingScreen();

    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Products ')),
      ),
      body: ListView.builder(
        itemCount: productsService.products
            .length, //la instancia products de tipo lista del modelo Product debe indicar en la propiedad itemcount que debe tomar los datos hasta la posicion .length
        itemBuilder: (BuildContext context, int index) => GestureDetector(
          onTap: () {
            productsService.selectedProduct = productsService.products[index].copy();
            Navigator.pushNamed(context, 'product');
          },
          child: ProductCard(
              product: productsService.products[index] // product (instancia de modelo Product) products(Instancia de tipo lista del modelo Product {[como es una lista esta contiene el index sobre el cual se tomaran los datos]})
              ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {},
      ),
    );
  }
}
